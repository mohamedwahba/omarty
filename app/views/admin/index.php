<?php require APPROOT . '/views/inc/header.php'; ?>
<?php if(isset($_SESSION['user_id'])) : ?>
<?php flash('error') ?>
<?php //print_r($data['paymentMonth']) ?>
    <div class="row">
        <div class="col-md-6">
            <h1>Add Member</h1>
        </div>
        <div class="col-md-6">
            <a href="<?php echo URLROOT; ?>/admin/add" class="btn btn-primary pull-right">
                <i class="fa fa-pencil"></i> Add Member
            </a>
        </div>
    </div>

<div class="row">
    <p><?php flash('add_message')?></p>

    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">Flat No</th>
            <th scope="col">Owner Name</th>
            <th scope="col">email</th>
            <th scope="col"class="text-center">Payment</th>
            <th scope="col" rowspan="2">Option</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data['title'] as $data) : ?>

        <tr>
            <th scope="row"><?php echo $data->flatNo?></th>
            <td><?php echo $data->ownerName?></td>
            <td><?php echo $data->email?></td>


            <td>  <a href="<?php echo URLROOT; ?>/admin/details/<?php echo $data->id?>" class="btn btn-success">
                    <i class="fa fa-pencil"></i> Payment
                </a></td>
            <td>  <a href="<?php echo URLROOT; ?>/admin/delete/<?php echo $data->id?>" class="btn btn-danger">
                    <i class="fa fa-trash"></i> Delete
                </a></td>


        </tr>
        <?php endforeach; ?>


        </tbody>
    </table>
</div>


<?php else : ?>
    <div class="container">
        <h1 class="display-3">You are not allowed</h1>
    </div>
<?php endif; ?>

<?php require APPROOT . '/views/inc/footer.php'; ?>
