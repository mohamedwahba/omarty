<?php require APPROOT . '/views/inc/header.php'; ?>
<?php if (isset($_SESSION['user_id'])) : ?>

    <div class="row">
        <h1>Details</h1>

        <div class="row col-12">
            <div class="col-12">
                <h3>Month</h3>
                <table class="table table-striped">
                    <tr>
                        <th>Owner Name</th>
                        <th>Flat Number</th>
                        <th>description of Payment</th>
                        <th>amount $</th>
                        <th>type</th>
                        <th>option</th>
                    </tr>
                    <?php foreach ($data['title'] as $data): ?>
                        <tr>
                            <td><?php echo $data->ownerName ?></td>
                            <td><?php echo $data->flatNo ?></td>
                            <td><?php echo $data->description ?></td>
                            <td><?php echo $data->amount ?></td>
                            <td><?php echo $data->type ?></td>
                            <td><a href="<?php echo URLROOT; ?>/admin/changeStatePaid/<?php echo $data->id ?>"
                                   class="btn btn-success">
                                    <i class="fa fa-trash"></i> Pay
                                </a></td>
                            </td>
                        </tr>
                    <?php endforeach; ?>


                </table>
            </div>

        </div>

    </div>


<?php else : ?>
    <div class="container">
        <h1 class="display-3">You are not allowed</h1>
    </div>
<?php endif; ?>

<?php require APPROOT . '/views/inc/footer.php'; ?>
