<?php require APPROOT . '/views/inc/header.php'; ?>
    <a href="<?php echo URLROOT; ?>/admin" class="btn btn-light"><i class="fa fa-backward"></i> Back</a>
    <?php flash('flat_err') ?>
    <div class="card card-body bg-light mt-5">
        <h2>Add Member</h2>
        <p>Add  Member with this form</p>
        <form action="<?php echo URLROOT; ?>/admin/add" method="post">
            <div class="form-group">
                <label for="email">Email: <sup>*</sup></label>
                <input type="email" name="email" class="form-control form-control-lg <?php echo (!empty($data['email_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['email']; ?>">
                <span class="invalid-feedback"><?php echo $data['email_err']; ?></span>
            </div>
            <div class="form-group">
                <label for="flatNo">Flat No: <sup>*</sup></label>
                <input type="number" name="flatNo" class="form-control form-control-lg <?php echo (!empty($data['flatNo_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['flatNo']; ?>">
                <span class="invalid-feedback"><?php echo $data['flatNo_err']; ?></span>
            </div>

            <div class="form-group">
                <label for="ownerName">Owner Name: <sup>*</sup></label>
                <input type="text" name="ownerName" class="form-control form-control-lg <?php echo (!empty($data['ownerName_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['ownerName']; ?>">
                <span class="invalid-feedback"><?php echo $data['ownerName_err']; ?></span>
            </div>




            <input type="submit" class="btn btn-success" value="Submit">
        </form>
    </div>
<?php require APPROOT . '/views/inc/footer.php'; ?>