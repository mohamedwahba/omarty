<?php require APPROOT . '/views/inc/header.php'; ?>
    <a href="<?php echo URLROOT; ?>/payouts" class="btn btn-light"><i class="fa fa-backward"></i> Back</a>
    <div class="card card-body bg-light mt-5">
        <h2>Add Payout</h2>
        <p>Add Payout with this form</p>
        <form action="<?php echo URLROOT; ?>/payouts/add" method="post">
            <div class="form-group">
                <label for="description">Description: <sup>*</sup></label>
                <textarea name="description"  cols="30" rows="10"class="form-control <?php echo (!empty($data['description_err'])) ? 'is-invalid' : ''; ?>">
                    <?php echo $data['description']; ?>
                </textarea>
                <span class="invalid-feedback"><?php echo $data['description_err']; ?></span>
            </div>
            <div class="form-group">
                <label for="amount">Amount $: <sup>*</sup></label>
                <input type="number" name="amount"
                       class="form-control form-control-lg <?php echo (!empty($data['amount_err'])) ? 'is-invalid' : ''; ?>"
                       value="<?php echo $data['amount']; ?>">
                <span class="invalid-feedback"><?php echo $data['amount_err']; ?></span>
            </div>

            <div class="form-group">
                <label for="member_id">Members: <sup>*</sup></label>
                <select name="member_id" class="form form-control">
                    <?php foreach ($data['member_id'] as $member): ?>
                        <option value="<?php echo $member->id?>"><?php echo $member->ownerName?></option>
                    <?php endforeach; ?>
                </select>
                <span class="invalid-feedback"><?php echo $data['member_id_err']; ?></span>
            </div>

            <input type="submit" class="btn btn-success form-control" value="Submit">
        </form>
    </div>
<?php require APPROOT . '/views/inc/footer.php'; ?>