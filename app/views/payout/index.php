<?php require APPROOT . '/views/inc/header.php'; ?>
<?php if(isset($_SESSION['user_id'])) : ?>
    <?php flash('add_payment')?>
    <?php flash('error_payment')?>
    <div class="row">


        <div class="col-md-4">
            <a href="<?php echo URLROOT; ?>/payouts/show" class="btn btn-primary">
                <i class="fa fa-pencil"></i> Payment in Period
            </a>
        </div>
        <div class="col-md-4">
            <a href="<?php echo URLROOT; ?>/payouts/add" class="btn btn-primary ">
                <i class="fa fa-pencil"></i> Add PayOut
            </a>


        </div>
        <div class="col-md-4">
            <p class="alert alert-danger ">Balance</p>
            <p class="alert alert-danger "><?php echo $data['balance']->value?></p>
        </div>
    </div>
    <hr>

    <div class="row">
        <h3>All Payment</h3>

        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Description</th>
                <th scope="col">amount</th>
                <th scope="col">date</th>
                <th scope="col">member Name</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data['title'] as $payment):?>
            <tr>
                <td><?php echo $payment->description?></td>
                <td><?php echo $payment->amount?></td>
                <td><?php echo $payment->date?></td>
                <td><?php echo $payment->ownerName?></td>
            </tr>
        <?php endforeach; ?>



            </tbody>
        </table>
    </div>


<?php else : ?>
    <div class="container">
        <h1 class="display-3">You are not allowed</h1>
    </div>
<?php endif; ?>

<?php require APPROOT . '/views/inc/footer.php'; ?>
