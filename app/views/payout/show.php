<?php require APPROOT . '/views/inc/header.php'; ?>
    <a href="<?php echo URLROOT; ?>/payouts" class="btn btn-light"><i class="fa fa-backward"></i> Back</a>
    <div class="card card-body bg-light mt-5">

        <form action="<?php echo URLROOT; ?>/payouts/show" method="post">
            <div class="form-group">
                <label for="description">from: <sup>*</sup></label>
                <input type="date" class="form-control <?php echo (!empty($data['from_err'])) ? 'is-invalid' : ''; ?>" name="from"value="<?php echo $data['from']; ?>">


                <span class="invalid-feedback"><?php echo $data['from_err']; ?></span>
            </div>

            <div class="form-group">
                <label for="description">to: <sup>*</sup></label>
                <input type="date" class="form-control <?php echo (!empty($data['to_err'])) ? 'is-invalid' : ''; ?>" name="to"value="<?php echo $data['to']; ?>">

                <span class="invalid-feedback"><?php echo $data['to_err']; ?></span>
            </div>



            <input type="submit" class="btn btn-success form-control" value="Submit">
        </form>
    </div>


<?php require APPROOT . '/views/inc/footer.php'; ?>