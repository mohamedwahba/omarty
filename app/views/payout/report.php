<?php require APPROOT . '/views/inc/header.php'; ?>
<?php if(isset($_SESSION['user_id'])) : ?>


    <div class="row">
        <h3>Payment in period </h3>

        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Description</th>
                <th scope="col">amount</th>
                <th scope="col">date</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data as $payment):?>
                <tr>
                    <td><?php echo $payment->description?></td>
                    <td><?php echo $payment->amount?></td>
                    <td><?php echo $payment->date?></td>
                </tr>
            <?php endforeach; ?>



            </tbody>
        </table>
    </div>


<?php else : ?>
    <div class="container">
        <h1 class="display-3">You are not allowed</h1>
    </div>
<?php endif; ?>

<?php require APPROOT . '/views/inc/footer.php'; ?>
