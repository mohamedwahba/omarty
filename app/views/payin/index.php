<?php require APPROOT . '/views/inc/header.php'; ?>
<?php if(isset($_SESSION['user_id'])) : ?>

<!--    --><?php //print_r($data) ?>
    <div class="row">
        <div class="col-md-6">
        </div>
        <div class="col-md-6">
            <a href="<?php echo URLROOT; ?>/payins/add" class="btn btn-primary pull-right">
                <i class="fa fa-pencil"></i> Add PayIn
            </a>
        </div>
    </div>

    <div class="row">
        <h3>All Payment</h3>

        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Description</th>
                <th scope="col">amount</th>
                <th scope="col">date</th>
                <th scope="col">Type</th>
                <th scope="col">Owner Name</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data['title'] as $payment):?>
                <tr>
                    <td><?php echo $payment->description?></td>
                    <td><?php echo $payment->amount?></td>
                    <td><?php echo $payment->date?></td>
                    <td><?php echo $payment->type?></td>
                    <td><?php echo $payment->ownerName?></td>
                </tr>
            <?php endforeach; ?>



            </tbody>
        </table>
    </div>


<?php else : ?>
    <div class="container">
        <h1 class="display-3">You are not allowed</h1>
    </div>
<?php endif; ?>

<?php require APPROOT . '/views/inc/footer.php'; ?>
