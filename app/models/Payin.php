<?php
class Payin
{
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function getPayments()
    {
        $this->db->query('SELECT p.id,p.description,p.date,p.amount,p.type,m.ownerName
FROM pay_in p ,members m 
WHERE m.id = p.member_id');
        return $this->db->resultSet();
    }


    public function addPayment($data)
    {
        $this->db->query('INSERT INTO pay_in(description , amount, type, member_id) VALUES (:description, :amount, :type, :member_id)');

        $this->db->bind(':description', $data['description']);
        $this->db->bind(':amount', $data['amount']);
        $this->db->bind(':type', $data['type']);
        $this->db->bind(':member_id', $data['member_id']);

        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }

    }


}