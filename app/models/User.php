<?php

class User
{
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function getUsersByEmail($email)
    {

        $this->db->query("SELECT * FROM members WHERE email = :email");
        $this->db->bind(':email', $email);

        $row = $this->db->single();
        // check row
        if ($this->db->rowCount() > 0) {

            return true;
        } else {
            return false;
        }
    }


    public function login($email, $password)
    {

        $this->db->query('SELECT * FROM members WHERE email = :email');
        $this->db->bind(':email', $email);

        $row = $this->db->single();

        $hashed_password = $row->password;

        if (($password == $hashed_password) && $row->role == 1) {
            return $row;
        } else {
            return false;
        }

    }

    public function getFlats()
    {
        $this->db->query('SELECT * FROM members');
        return $this->db->resultSet();
    }

    public function checkFlat($flat)
    {
        $this->db->query('SELECT flatNo FROM members WHERE flatNo =:flat');
        $this->db->bind(':flat',$flat);
        return $result = $this->db->resultSet();

    }



    public function addMember($data)
    {
        $flat = $data['flatNo'];
        if ($this->checkFlat($flat)){
            return false;
        }
        {


            $this->db->query('INSERT INTO members(flatNo , ownerName, email, role) VALUES (:flatNo, :ownerName, :email, :role)');
            $this->db->bind(':flatNo', $data['flatNo']);
            $this->db->bind(':ownerName', $data['ownerName']);
            $this->db->bind(':email', $data['email']);
//        $this->db->bind(':pay_in_id', $data['pay_in_id']);
            $this->db->bind(':role', $data['role']);

            // Execute
            if ($this->db->execute()) {
                return true;
            } else {
                return false;
            }

        }
    }


    public function deleteUser($id)
    {
        if ($id == $_SESSION['user_id']) {
            flash('error', 'cant remove admin');

        } else {
            $this->db->query('DELETE FROM pay_in WHERE member_id = :id; DELETE FROM pay_out WHERE member_id = :id;DELETE FROM members WHERE id = :id');

            $this->db->bind(':id', $id);

            if ($this->db->execute()) {
                return json_encode(array('success' => 1));
            } else {
                return json_encode(array('success' => 0));
            }
        }


    }

    public function changeState($id)
    {
        $this->db->query('SELECT amount FROM pay_in p WHERE p.id = :id ');
        $this->db->bind(':id', $id);
        $row = $this->db->single();

        $balance = $row->amount;






        $this->db->query('UPDATE box SET value =value + :balance;UPDATE pay_in p
                                SET p.flag = 1
                                wHERE p.id = :id;');
        $this->db->bind(':id', $id);
        $this->db->bind(':balance', $balance);


        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }

    }





    public function getallMembers()
    {
        $this->db->query('SELECT id,ownerName
                                FROM members');
        return $this->db->resultSet();
    }

    public function getAllPayment()
    {
        $this->db->query('SELECT id,description
                                FROM pay_in');
        return $this->db->resultSet();
    }


    public function getAllPaymentMonth($id)
    {
        $this->db->query('SELECT m.ownerName,m.flatNo,p.description,p.amount,p.type,p.date ,p.id
                              FROM members m , pay_in p WHERE m.id = p.member_id AND m.id = :id AND p.flag = :flag');
        $this->db->bind(':id', $id);
        $this->db->bind(':flag','0');
         return $this->db->resultSet();

    }

//
//    public function getAllPaymentEmergency($id)
//    {
//        $this->db->query('SELECT m.ownerName,m.flatNo,p.description,p.amount,p.type,p.date
//                              FROM members m , pay_in p WHERE m.id = p.member_id AND m.id = :id and p.type = :type, AND p.flag = :flag');
//        $this->db->bind(':id', $id);
//        $this->db->bind(':type','emergency');
//        $this->db->bind(':flag','0');
//
//        return $this->db->resultSet();
//
//    }




}