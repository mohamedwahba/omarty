<?php

class Payout
{
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function getPayments()
    {
        $this->db->query('SELECT m.id,description,amount,date,ownerName
                                FROM pay_out p , members m 
                                WHERE m.id = p.member_id');
        return $this->db->resultSet();
    }


    public function balance()
    {

        $this->db->query('SELECT value FROM box WHERE id = :id');
        $this->db->bind(':id', 1);

        return $this->db->single();


    }

    public function addPayment($data)
    {
        $row = $this->balance();

        if ($row->value - $data['amount'] > 0) {


            $this->db->query('Update box SET value = value - :amount;INSERT INTO pay_out(description , amount, member_id) VALUES (:description, :amount, :member_id)');
            $this->db->bind(':description', $data['description']);
            $this->db->bind(':amount', $data['amount']);
            $this->db->bind(':member_id', $data['member_id']);

            // Execute
            if ($this->db->execute()) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }

    }

    public function report($data)

    {
        $from = $data['from'];
        $to = $data['to'];
        $this->db->query('SELECT * from pay_out WHERE date BETWEEN :from AND :to');
        $this->db->bind(':from', $from);
        $this->db->bind(':to', $to);
       return $this->db->resultSet();

    }


}