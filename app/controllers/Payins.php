<?php
class Payins extends Controller
{
    private $userModel;

    public function __construct()
    {
        $this->userModel = $this->model('Payin');

    }

    public function index()
    {
        $payment = $this->userModel->getPayments();

        $data = [
            'title' => $payment,
        ];


        $this->view('payin/index', $data);
    }


    public function add()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'description' => trim($_POST['description']),
                'amount' => trim($_POST['amount']),
                'type' => trim($_POST['type']),
                'member_id' => trim($_POST['member_id']),
                'description_err' => '',
                'amount_err' => '',
                'type_err' => ''
            ];


            // Validate data
            if (empty($data['description'])) {
                $data['description_err'] = 'Please enter description';
            }
            if (empty($data['amount'])) {
                $data['amount_err'] = 'Please enter amount';
            }

            if (empty($data['type'])) {
                $data['type_err'] = 'Please choose type';
            }
            if (empty($data['member_id'])) {
                $data['member_id_err'] = 'Please enter owner name';
            }


            // Make sure no errors
            if (empty($data['description_err']) &&empty($data['member_id_err']) && empty($data['amount_err']) && empty($data['type_err'])) {
                // Validated


                if ($this->userModel->addPayment($data)) {

                    flash('add_payment', 'Payment Added');
                    header('location: ' . URLROOT . '/payins/index');
                } else {
                    die('Something went wrong');
                }
            } else {
                // Load view with errors
                $this->view('payin/add', $data);
            }

        } else {
            $this->userModel = $this->model('User');

            $data = [
                'description' => '',
                'amount' => '',
                'type' => '',
                'member_id' => $this->userModel->getallMembers(),

            ];

            $this->view('payin/add', $data);
        }
    }
}
