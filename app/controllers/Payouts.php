<?php

class Payouts extends Controller
{
    private $userModel;

    public function __construct()
    {
        $this->userModel = $this->model('Payout');

    }

    public function index()
    {
        $payment = $this->userModel->getPayments();
        $balance = $this->userModel->balance();

        $data = [
            'title' => $payment,
            'balance' => $balance,
        ];


        $this->view('payout/index', $data);
    }

    public function add()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'description' => trim($_POST['description']),
                'amount' => trim($_POST['amount']),
                'member_id' => trim($_POST['member_id']),
                'description_err' => '',
                'amount_err' => '',
                'member_id_err' => ''
            ];


            // Validate data
            if (empty($data['description'])) {
                $data['description_err'] = 'Please enter description';
            }
            if (empty($data['amount'])) {
                $data['amount_err'] = 'Please enter amount';
            }

            if (empty($data['member_id'])) {
                $data['member_id_err'] = 'Please enter owner name';
            }

            // Make sure no errors
            if (empty($data['description_err']) && empty($data['amount_err']) && empty($data['member_id_err'])) {
                // Validated

                if ($this->userModel->addPayment($data)) {

                    flash('add_payment', 'Payment Added');
                    header('location: ' . URLROOT . '/payouts/index');
                } else {


                    flash('error_payment', 'your balance is not enough', 'alert alert-danger');
                    header('location: ' . URLROOT . '/payouts/index');
//                    die('Something went wrong or your balance is not enough');
                }
            } else {
                // Load view with errors
                $this->view('payout/add', $data);
            }

        } else {
            $this->userModel = $this->model('User');

            $data = [
                'description' => '',
                'amount' => '',
                'member_id' => $this->userModel->getallMembers(),
            ];

            $this->view('payout/add', $data);
        }
    }

    public function show()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'from' => trim($_POST['from']),
                'to' => trim($_POST['to']),
                'from_err' => '',
                'to_err' => ''
            ];

            if (empty($data['from'])) {
                $data['from_err'] = 'Please enter from';
            }
            if (empty($data['to'])) {
                $data['to_err'] = 'Please enter to';
            }

            if (empty($data['from_err']) && empty($data['to_err'])) {
                $data= $this->userModel->report($data);
//                print_r($report);
//                flash('data', 'Payment Added');
                $this->view('payout/report', $data);


            }else
            {
                $this->view('payout/show', $data);
            }
        }else
        {

            $data = [
                'from' => '',
                'to' => '',
                'from_err' => '',
                'to_err' => ''
            ];
            $this->view('payout/show',$data);

        }


    }


}