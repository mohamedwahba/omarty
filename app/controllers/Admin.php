<?php

class Admin extends Controller
{
    private $userModel;

    public function __construct()
    {
        $this->userModel = $this->model('User');

    }

    public function index()
    {

        $flat = $this->userModel->getFlats();


        $data = [
            'title' => $flat,
        ];

        $this->view('admin/index', $data);
    }

    public function add()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'email' => trim($_POST['email']),
                'flatNo' => trim($_POST['flatNo']),
                'ownerName' => trim($_POST['ownerName']),
//                'pay_in_id' => trim($_POST['pay_in_id']),
                'role' => '0',
                'email_err' => '',
                'flatNo_err' => '',
                'ownerName_err' => '',
                'pay_in_id_err'=>''
            ];

//            print_r($data);


            // Validate data
            if (empty($data['email'])) {
                $data['email_err'] = 'Please enter email';
            }
            if (empty($data['flatNo'])) {
                $data['flatNo_err'] = 'Please enter flat No';
            }

            if (empty($data['ownerName'])) {
                $data['ownerName_err'] = 'Please enter owner name';
            }
//            if (empty($data['pay_in_id'])) {
//                $data['pay_in_id_err'] = 'Please enter Payment';
//            }

            // Make sure no errors
            if (empty($data['email_err']) && empty($data['flatNo_err']) && empty($data['ownerName_err'])) {
                // Validated
//                print_r($data);
                if ($this->userModel->addMember($data)) {

                    flash('add_message', 'Member Added');
                    header('location: ' . URLROOT . '/admin/index');
                } else {
                    flash('flat_err', 'flat not empty');


                    $this->view('admin/add', $data);
                }
            } else {
                // Load view with errors
                $this->view('admin/add', $data);
            }

        } else {

            $data = [
                'email' => '',
                'flatNo' => '',
                'ownerName' => '',
//                'pay_in_id' => $this->userModel->getAllPayment(),

            ];

            $this->view('admin/add', $data);
        }
    }

    public function delete($id)
    {

        if ($this->userModel->deleteUser($id)) {
            flash('delete_message', 'member Removed');
            header('location: ' . URLROOT . '/admin/index');


        } else {
            header('location: ' . URLROOT . '/admin/index');

        }

    }

    public function changeStatePaid($id)
    {
        $this->userModel->changeState($id);
        header('location: ' . URLROOT . '/admin/index');

    }

    public function details($id)
    {

        $paymentMonth = $this->userModel->getAllPaymentMonth($id);


        $data = [
            'title' => $paymentMonth,
        ];

        $this->view('admin/details', $data);
    }


}