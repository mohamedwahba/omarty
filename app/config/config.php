<?php
  // DB Params
  define('DB_HOST', 'localhost');
  define('DB_USER', 'root');
  define('DB_PASS', '');
  define('DB_NAME', 'omarty');

  // App Root
  define('APPROOT', dirname(dirname(__FILE__)));
  // URL Root
  define('URLROOT', 'http://localhost/Omarty');
  // Site Name
  define('SITENAME', 'Omarty');
  // App Version
  define('APPVERSION', '1.0.0');